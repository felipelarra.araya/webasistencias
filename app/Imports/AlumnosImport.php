<?php
namespace App\Imports;
use App\Alumno;
//use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\ToModel;

class AlumnosImport implements ToModel

{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    private $idCurso;

    public function __construct(int $idCurso) 
    {
         
        $this->idCurso  = $idCurso; 
        
    }
    public function model(array $row)
    {

       
        return new Alumno([
            'nombre' =>  $row[0],
            'apellidos'=> $row[1],
            'rut'      => $row[2],
            'id_curso' => $this->idCurso
            
        ]);
    }
/*
    public function rules(): array
    {
        //Validar que el rut ingresado no exista, si esta borrado con softdelete permite registrar
        return [
            '2' => 'unique:alumnos,rut,NULL,id,deleted_at,NULL',
           
        ];

        
        
    }

    public function customValidationMessages()
{
    return [
        '2.in' => 'El alumno ya se encuentra registrado',
    ];
}

public function customValidationAttributes()
{
    return ['2' => 'Rut'];
}
*/
}
