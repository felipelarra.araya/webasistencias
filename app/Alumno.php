<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alumno extends Model
{
    use SoftDeletes; 
    protected $fillable = ['nombre','apellidos','rut','id_curso'];
    protected $table = "alumnos";
    protected $dates = ['deleted_at'];
    protected $hidden =   ['created_at','updated_at'];
}
