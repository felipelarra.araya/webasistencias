<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class ReportesController extends Controller
{
    public function index($idCurso)
    {
        
        $asignaturas = \DB::table('asignaturas_cursos')
        ->join('periodos','periodos.id' , '=' ,'asignaturas_cursos.id_periodo')
        ->join('cursos','cursos.id' , '=' ,'asignaturas_cursos.id_curso')
        ->join('asignaturas','asignaturas.id' , '=' ,'asignaturas_cursos.id_asignatura')
        ->select('asignaturas_cursos.id','asignaturas_cursos.id_asignatura','asignaturas_cursos.id_curso','cursos.nombre as nombreCurso', 'asignaturas.nombre')
        ->where('asignaturas_cursos.id_curso','=', $idCurso)
        ->where('periodos.id','=',\Session::get('idPeriodo'))
        ->whereNull('asignaturas_cursos.deleted_at')
        ->orderBy('asignaturas_cursos.id','ASC')
        ->get();

        return view("layouts.reportes.reportes", ['asignaturas' => $asignaturas , 'idCursoActual' => $idCurso]); 
    }

    public function Buscar(Request $request)
    {

        
        $idCursoActual= $request->idCursoActual;
        $fechaInicio = $request->fechaInicio;
        $fechaFin = $request->fechaFin;
        $idAsignatura = $request->asignatura;
        $rango = 0;

		$rango = ((strtotime($fechaFin)-strtotime($fechaInicio))+(24*60*60)) /(24*60*60);
			
        

    
        $alumnos = \DB::table('alumnos')
                    ->select('alumnos.id','alumnos.nombre' , 'alumnos.apellidos')
                    ->whereNull('deleted_at')
                    ->where('id_curso','=', $idCursoActual)
                    ->orderBy('id','DESC')
                    ->get();

        
        $asignaturas = \DB::table('asignaturas_cursos')
        ->join('periodos','periodos.id' , '=' ,'asignaturas_cursos.id_periodo')
        ->join('cursos','cursos.id' , '=' ,'asignaturas_cursos.id_curso')
        ->join('asignaturas','asignaturas.id' , '=' ,'asignaturas_cursos.id_asignatura')
        ->select('asignaturas_cursos.id','asignaturas_cursos.id_asignatura','asignaturas_cursos.id_curso','cursos.nombre as nombreCurso', 'asignaturas.nombre')
        ->where('asignaturas_cursos.id_curso','=', $idCursoActual)
        ->where('periodos.id','=',\Session::get('idPeriodo'))
        ->whereNull('asignaturas_cursos.deleted_at')
        ->orderBy('asignaturas_cursos.id','ASC')
        ->get();

        $asignaturaBuscada = \DB::table('asignaturas')
        ->select('id')
        ->whereNull('deleted_at')
        ->where('id','=', $idAsignatura)
        ->orderBy('id','DESC')
        ->first(1);
        
        $asisten = \DB::table('asistencias')
        ->selectRaw('count(*) as presente_ausente, estado, case
                when estado = 1 then "Presente"
                when estado = 2 then "Ausente"
                end as estado_asistencia')

        ->where('id_curso', '=', $idCursoActual)
        ->where('id_asignatura', '=', $idAsignatura)
        ->whereBetween('fecha', [$fechaInicio, $fechaFin])
        ->groupBy('estado')
        ->get();

         
        $view = view('layouts.reportes.resultados', compact('alumnos','fechaInicio','fechaFin','idCursoActual','idAsignatura','rango','asisten'))->render();
        return response()->json($view);
    }

    

    
}
