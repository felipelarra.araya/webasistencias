<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Auth;
use Hash;
class UserController extends Controller
{



    public function __construct()
    {
        $this->middleware('preventBackHistory');
        $this->middleware('auth');
        
    }

    public function index()
    {
        return view('layouts.user.cambiarContrasena'); 
    }


    public function cambiarContraseña(Request $request)
    {
        
        $messages = [
            'nuevoPassword.confirmed' => 'Las contraseñas no coinciden',
            'passwordActual.required' => 'Campo contraseña actual es requerido',
            'password.required' => 'Campo contraseña nueva es requerido'
            
            
        ];
        $validator = Validator::make($request->all(),[
            'passwordActual'=>'required',
            'password'=>'confirmed|required|min:8',
            
           
        ],$messages);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{

            if(Hash::check($request->passwordActual,  Auth::user()->password )){
                $user = User::find(Auth::user()->email);
                $user = new User;
                $user->where('email','=',Auth::user()->email)
                     ->update(['password'=> bcrypt($request->password)]);
                return back()->with('Listo','Datos actualizados correctamente');
            }
            else{
            return back()
            ->withInput()
            ->with('ErrorContraseña','Contraseña actual incorrecta')
            ->withErrors($validator);
            }
              
           
        }
    }


    public function ingresarUsuario(Request $request)
    {        
    
        $validator = Validator::make($request->all(),[
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{
            
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'nivel' => 0,
                'password' => Hash::make($request->password),
            ]);
            return back()->with('Listo','Ingresado correctamente');
        
        }
        
    }
}
