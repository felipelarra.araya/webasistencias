<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteExport;
use App\Exports\ReporteListadoExport;


class ReportePdfController extends Controller
{


    public function ExportarExcel($fechaInicio,$idAsignatura,$idCurso,$fechaFin)
    {
        $curso = \DB::table('cursos')
                    ->select('nombre')
                    ->whereNull('deleted_at')
                    ->where('id','=', $idCurso)
                    ->first();
      
        return Excel::download(new ReporteExport($fechaInicio,$idCurso,$idAsignatura,$fechaFin), 'ReporteAsistencia_'.$curso->nombre. '.xlsx');
       
    }


    public function ExportarTodoExcel($fechaInicio,$idAsignatura,$idCurso,$fechaFin)
    {

        $curso = \DB::table('cursos')
                    ->select('nombre')
                    ->whereNull('deleted_at')
                    ->where('id','=', $idCurso)
                    ->first();

                    $asignaturas = \DB::table('asignaturas_cursos')
                    ->join('asignaturas','asignaturas.id' , '=' ,'asignaturas_cursos.id_asignatura')
                    ->select('asignaturas.id')
                    ->where('asignaturas_cursos.id_curso','=', $idCurso)
                    ->where('asignaturas_cursos.id_periodo','=',\Session::get('idPeriodo'))
                    ->whereNull('asignaturas_cursos.deleted_at')
                    ->orderBy('asignaturas_cursos.id','ASC')
                    ->get();
                    
                    $idAsignaturas = array_column($asignaturas->toArray(),'id');
                    $asignaturasCount = count($asignaturas);
                    
                    
                   
         
             
        //return Excel::download(new ReporteExport($fechaInicio,$idCurso,$idAsignatura,$fechaFin), 'ReporteAsistencia_'.$curso->nombre. '.xlsx');
        return Excel::download(new ReporteListadoExport($idAsignaturas,$asignaturasCount,$fechaInicio,$idCurso,$fechaFin), 'ReporteAsistencia_'.$curso->nombre. '.xlsx');
    }

    public function DescargarPdf($fechaInicio,$idAsignatura,$idCursoActual,$fechaFin)
    {
        $rango = 0;
        $rango = ((strtotime($fechaFin)-strtotime($fechaInicio))+(24*60*60)) /(24*60*60);
     
        $alumnos = \DB::table('alumnos')
                    ->select('alumnos.id','alumnos.nombre' , 'alumnos.apellidos')
                    ->whereNull('deleted_at')
                    ->where('id_curso','=', $idCursoActual)
                    ->orderBy('id','DESC')
                    ->get();

        
        $pdf = \PDF::loadView('layouts.reportes.reporteExcel',compact('rango', 'alumnos','fechaInicio','idAsignatura','idCursoActual','fechaFin'));
        
        $pdf->setPaper('letter', 'landscape');
        return $pdf->download('ReporteAsistencia.pdf');
    }
}
