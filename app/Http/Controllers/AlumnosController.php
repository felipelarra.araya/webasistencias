<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use App\Asignatura;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\AlumnosImport;
use Illuminate\Support\Facades\Storage;


class AlumnosController extends Controller
{

    public function __construct()
    {	$this->middleware('preventBackHistory');
        $this->middleware('auth');
        
    }
    public function index($id)
    {
        \Session::put('idCurso',$id);
        $alumnos = \DB::table('alumnos')
        ->join('cursos','cursos.id' , '=' ,'alumnos.id_curso')
        ->join('periodos','periodos.id' , '=' ,'cursos.id_periodo')
        ->select('alumnos.id','alumnos.nombre','alumnos.apellidos','alumnos.rut','alumnos.id_curso')
        ->where('cursos.id','=', $id)
        ->where('periodos.id','=',\Session::get('idPeriodo'))
        ->whereNull('alumnos.deleted_at')
        ->orderBy('alumnos.id','DESC')
        ->paginate(10);


        //Asignaturas que tienen asignadas el cursos

        $asignaturas_cursos = \DB::table('asignaturas_cursos')
        ->join('asignaturas','asignaturas.id' , '=' ,'asignaturas_cursos.id_asignatura')
        ->whereNull('asignaturas_cursos.deleted_at')
        ->where('asignaturas_cursos.id_curso','=', $id)
        ->where('asignaturas_cursos.id_periodo','=',\Session::get('idPeriodo'))
        ->pluck('asignaturas_cursos.id_asignatura','asignaturas.nombre');
        
        
        //Las asignaturas que no estan asignadas al curso
        $asignaturas = Asignatura::select('id','nombre')->whereNotIn('id', $asignaturas_cursos)->get();
       
        
        $curso = \DB::table('cursos')
        ->select('id','nombre')
        ->whereNull('deleted_at')
        ->where('cursos.id','=', $id)
        ->orderBy('id','DESC')
        ->get();
        return view("layouts.alumnos.alumnos", 
        ['alumnos' => $alumnos, 
        'curso' => $curso,
        'asignaturas_cursos' => $asignaturas_cursos,
        'asignaturas' => $asignaturas]);
    }

    public function getAlumnosAdd($idCurso)
    {
        $idCursoRecibido = $idCurso;
        return view('layouts.alumnos.nuevoAlumno')->with('idCurso',$idCursoRecibido); 
    }

    public function store(Request $request)
    {        

/*        $messages = [
            'rut.unique' => 'Usuario ingresado ya existe',

        ];
  
  */
        /* Se hace query para ver el alumno a ingresar si existe en el curso actual y periodo actual 
        si no existe en ese curso y periodo se inserta, si existe en tal curso y periodo se inserta 
        */

            $alumnosCursos = \DB::table('alumnos')
            ->join('cursos','cursos.id' , '=' ,'alumnos.id_curso')
            ->join('periodos','periodos.id' , '=' ,'cursos.id_periodo')
            ->select('alumnos.rut','cursos.nombre','periodos.id','periodos.nombre as periodo')
            ->where('alumnos.rut','=', $request->rut)
            ->where('periodos.id','=',\Session::get('idPeriodo'))
            ->whereNull('alumnos.deleted_at')
            ->get();

            $alumnosCursosCount = count($alumnosCursos);
            
            if($alumnosCursosCount > 0){

                return back()->with('ErrorRegistro','Alumno ya existe');
            }
            else{


        $validator = Validator::make($request->all(),[
            'nombre'=>'required',
            'apellidos'=>'required',
            'rut' => 'required',
            //'rut'=>'required|unique:alumnos,rut,null,id,deleted_at,null'
            //'rut'=>'required|unique:alumnos,rut,NULL,id,deleted_at,NULL',
            
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{
            
            $alumno = Alumno::create([
                'nombre'=>$request->nombre,
                'apellidos'=>$request->apellidos,
                'rut'=>$request->rut,
                'id_curso'=>$request->idCurso

            ]);
            return back()->with('Listo','Ingresado correctamente');
            //$request->session()->flash('Listo', 'Ingresado Correctamente');
            //return redirect()->route('alumnos', array('id' => $request->idCurso));
        
        }
    }
    }


    public function editarAlumno(Request $request)
    {
        
        $alumno = Alumno::find($request->id);
        $validator = Validator::make($request->all(),[
            'nombre'=>'required',
            'apellidos'=>'required',
            'rut'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{

        
              $alumno->nombre= $request->nombre;
              $alumno->apellidos= $request->apellidos;
              $alumno->rut= $request->rut;
              $alumno->save();
              return back()->with('Listo','Datos actualizado correctamente');
           
        }
    }

    public function getAlumnoDelete($id)
    {
        
        $alumno = Alumno::find($id);
        if($alumno->delete()){
            return back()->with('Listo','Registro eliminado exitosamente');
        }
    }

    public function subirExcel(Request $request)
    {
       $idCurso = $request->idCurso;
       $messages = [
        'excelAlumno.mimes' => 'Formato del archivo incorrecto',
    
    ];

        $validator = Validator::make($request->all(),[
            'excelAlumno'=>'required|mimes:xlsx,xls',
        ],$messages);

        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{
        
            $file = $request->file('excelAlumno');
        Excel::import(new AlumnosImport($idCurso), $file);

        return back()->with('Listo','Ingresado correctamente');
            /*try {
                $file = $request->file('excelAlumno');
        Excel::import(new AlumnosImport($idCurso), $file);

        return back()->with('Listo','Ingresado correctamente');
             }
             catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $failures = $e->failures();
                return view('layouts.alumnos.nuevoAlumno', compact('failures','idCurso'));
             }   

       */
    }
}



public function descargarExcel()
{
    $filename ='FormatoExcel.xlsx';
    $path = storage_path('app/'. $filename);
    
   
    
    return response()->download($path, $filename);
}

  
    }

