<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asistencia;
use Carbon\Carbon;


class AsistenciasController extends Controller
{

    public function __construct()
    {	
        $this->middleware('preventBackHistory');
        $this->middleware('auth');
        
    }
    public function index($idCurso,Request $request)
    {
      
        
            $fecha_actual = Carbon::now()->format('Y-m-d');
            
           $idCursoSession= \Session::get('idCurso');

        
           $fecha = $request->fecha;
           $asignatura  = $request->asignatura;
           $alumnos = \DB::table('asistencias')
                    ->join('alumnos','alumnos.id' , '=' ,'asistencias.id_alumno')
                    ->join('cursos','cursos.id' , '=' ,'asistencias.id_curso')
                    ->join('asignaturas','asignaturas.id' , '=' ,'asistencias.id_asignatura')
                    ->select('alumnos.id','alumnos.nombre','alumnos.apellidos','alumnos.rut','asistencias.estado','asistencias.id as asistenciaId')
                    ->where('asistencias.fecha','=', $fecha)
                    ->where('asistencias.id_asignatura','=',$asignatura)
                    ->where('asistencias.id_curso','=',$idCurso)
                    ->whereNull('asistencias.deleted_at')
                    ->orderBy('asistencias.id','ASC')
                    ->get(); 
       

                $asignaturas = \DB::table('asignaturas_cursos')
                ->join('periodos','periodos.id' , '=' ,'asignaturas_cursos.id_periodo')
                ->join('cursos','cursos.id' , '=' ,'asignaturas_cursos.id_curso')
                ->join('asignaturas','asignaturas.id' , '=' ,'asignaturas_cursos.id_asignatura')
                ->select('asignaturas_cursos.id','asignaturas_cursos.id_asignatura','asignaturas_cursos.id_curso','cursos.nombre as nombreCurso', 'asignaturas.nombre')
                ->where('asignaturas_cursos.id_curso','=', $idCursoSession)
                ->where('periodos.id','=',\Session::get('idPeriodo'))
                ->whereNull('asignaturas_cursos.deleted_at')
                ->orderBy('asignaturas_cursos.id','ASC')
                ->get();

                if($asignatura == null){
                    return view("layouts.asistencias.asistencias", 
                        ['idCursoActual' => $idCursoSession,
                        'asignaturas'=> $asignaturas,
                        'alumnos' => $alumnos,
                        'fecha' => $fecha,
                        'asignaturaBuscada' => $asignatura]); 
                }
                else{
         
        $asistenciasCount = count($alumnos);
        
        if($asistenciasCount > 0){  


                    return view("layouts.asistencias.asistencias", 
                        ['idCursoActual' => $idCursoSession,
                        'asignaturas'=> $asignaturas,
                        'alumnos' => $alumnos,
                        'fecha' => $fecha,
                        'asignaturaBuscada' => $asignatura]); 
        }else{

            $alumnosId = \DB::table('alumnos')
            ->select('alumnos.id')
            ->whereNull('deleted_at')
            ->where('id_curso','=', $idCurso)
            ->orderBy('id','DESC')
            ->get();
        
          

        }
        return view("layouts.asistencias.asistencias", 
        ['idCursoActual' => $idCursoSession,
        'asignaturas'=> $asignaturas,
        'alumnos' => $alumnos,
        'fecha' => $fecha,
        'asignaturaBuscada' => $asignatura]); 
         


        }
                
                   
                             
            }
        


    public function buscarAsistencia(Request $request)
    {

        $fecha = $request->fecha;
        $asignatura = $request->asignatura;
        $idCurso= $request->idCursoActual;

        $data = \DB::table('asistencias')
        ->join('alumnos','alumnos.id' , '=' ,'asistencias.id_alumno')
        ->join('cursos','cursos.id' , '=' ,'asistencias.id_curso')
        ->join('asignaturas','asignaturas.id' , '=' ,'asistencias.id_asignatura')
        ->select('alumnos.nombre' ,'alumnos.id','alumnos.apellidos','alumnos.rut','asistencias.estado','asistencias.id as asistenciaId','asistencias.id_asignatura')
        ->where('asistencias.fecha','=', $fecha)
        ->where('asistencias.id_asignatura','=',$asignatura)
        ->where('asistencias.id_curso','=',$idCurso)
        ->whereNull('asistencias.deleted_at')
        ->orderBy('asistencias.id','ASC')
        ->get(); 
        $asistenciasCount = count($data);
        
        if($asistenciasCount > 0){   
        
        $view = view('layouts.asistencias.resultados', compact('data'))->render();
        return response()->json($view);
        }
        else{
              
            $data = \DB::table('alumnos')
            ->select('alumnos.*')
            ->whereNull('deleted_at')
            ->where('id_curso','=', $idCurso)
            ->orderBy('id','DESC')
            ->get();
        
           /*Pasamos por compact la fecha y la asignatura para poder recuperarlas como hidden en resultado2
            y poner registrar la asistencia por primera vez
           */
           
            $view = view('layouts.asistencias.resultados2', compact('data','fecha','asignatura'))->render();
            return response()->json($view);
           
         } 
        
    
    /*
        $fecha = $request->fecha;
        $asignatura = $request->asignatura;
        $idCurso= $request->idCursoActual;

        
        
        $asignaturas = \DB::table('asignaturas_cursos')
                    ->join('periodos','periodos.id' , '=' ,'asignaturas_cursos.id_periodo')
                    ->join('cursos','cursos.id' , '=' ,'asignaturas_cursos.id_curso')
                    ->join('asignaturas','asignaturas.id' , '=' ,'asignaturas_cursos.id_asignatura')
                    ->select('asignaturas_cursos.id','asignaturas_cursos.id_asignatura','asignaturas_cursos.id_curso','cursos.nombre as nombreCurso', 'asignaturas.nombre')
                    ->where('asignaturas_cursos.id_curso','=', $idCurso)
                    ->where('periodos.id','=',\Session::get('idPeriodo'))
                    ->whereNull('asignaturas_cursos.deleted_at')
                    ->orderBy('asignaturas_cursos.id','ASC')
                    ->get();

        $alumnos = \DB::table('asistencias')
                    ->join('alumnos','alumnos.id' , '=' ,'asistencias.id_alumno')
                    ->join('cursos','cursos.id' , '=' ,'asistencias.id_curso')
                    ->join('asignaturas','asignaturas.id' , '=' ,'asistencias.id_asignatura')
                    ->select('alumnos.nombre' ,'alumnos.id','alumnos.apellidos','alumnos.rut','asistencias.estado','asistencias.id as asistenciaId','asistencias.id_asignatura')
                    ->where('asistencias.fecha','=', $fecha)
                    ->where('asistencias.id_asignatura','=',$asignatura)
                    ->where('asistencias.id_curso','=',$idCurso)
                    ->whereNull('asistencias.deleted_at')
                    ->orderBy('asistencias.id','ASC')
                    ->get();  


        $asistenciasCount = count($alumnos);
        
        if($asistenciasCount > 0){   
        
           
            
            return view("layouts.asistencias.asistenciaDetalle", 
            ['alumnos' => $alumnos, 
            'asignaturas' => $asignaturas,
            'idCursoActual' => $idCurso,
            'fechaBuscada' => $fecha,
            'asignaturaBuscada' => $asignatura]); 
        }
        else{

            
            $alumnosId = \DB::table('alumnos')
            ->select('alumnos.id')
            ->whereNull('deleted_at')
            ->where('id_curso','=', $idCurso)
            ->orderBy('id','DESC')
            ->get();
        
            foreach ($alumnosId as $idAlumnos){
            $asistenciaRegistro = Asistencia::create([
                'estado'=>2,
                'id_alumno'=>$idAlumnos->id,
                'id_curso'=>$idCurso,
                'id_asignatura'=>$asignatura,
                'fecha'=>$fecha
           ]);
         } 


         return redirect()->route("detalleAsistencia", array('idCurso' => $idCurso , 'fecha' => $fecha , 'asignatura' => $asignatura));
         
            
       }
*/
        }
        
        public function enviarAsistencia(Request $request,$idCurso){
            
            
            \Session::put('idCurso',$idCurso);
            if($request->opcion == 'enviar'){
         
          
        $asistenciaId = $request->asistencia;
      
          foreach($asistenciaId as $idAsistencia => $e) {
              
              $asistencia = Asistencia::find($idAsistencia); 
              $asistencia->estado = $e;
              $asistencia->save();
              
          }
        return back()->with('Listo','Asistencia ingresada correctamente');
    }else{

                $asistenciaId = $request->asistencia;
                $fecha = $request->fecha;
                $asignatura = $request->asignatura;    
                foreach($asistenciaId as $idAlumno => $e) {
                    
                    $asistenciaRegistro = Asistencia::create([
                        'estado'=>$e,
                        'id_alumno'=>$idAlumno,
                        'id_curso'=>$idCurso,
                        'id_asignatura'=>$asignatura,
                        'fecha'=>$fecha
                   ]);
                  
                  
                    
                }
              return back()->with('Listo','Asistencia ingresada correctamente');
            }
        }

        
}   
