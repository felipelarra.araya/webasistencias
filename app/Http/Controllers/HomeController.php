<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Periodo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    public function __construct()
    {	
        $this->middleware('preventBackHistory');
    	$this->middleware('auth');
        $period = Periodo::select('id')->orderBy('id','DESC')->take(1)->first();
            if(!is_null($period))
                if(!(\Session::has('idPeriodo'))) 
                    \Session::put('idPeriodo',$period->id);
        
    }
    public function index()
    {
        $cursos = \DB::table('cursos')
        ->join('periodos','periodos.id' , '=' ,'cursos.id_periodo')
        ->select('cursos.id','cursos.nombre')
        ->where('periodos.id','=',\Session::get('idPeriodo'))
        ->whereNull('cursos.deleted_at')
        ->paginate(8);

        $asignaturas = \DB::table('asignaturas')
        ->select('asignaturas.id','asignaturas.nombre')
        ->whereNull('deleted_at')
        ->get();
        
        return view("layouts.home", ['cursos' => $cursos,'asignaturas'=> $asignaturas]);
        
    }
}
