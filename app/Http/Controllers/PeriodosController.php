<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Periodo;
use App\Curso;
use Auth;
class PeriodosController extends Controller
{

    public function __construct()
    {
        $this->middleware('preventBackHistory');
        $this->middleware('auth');
        
        
    }
    
    public function index()
    {
        $periodos = \DB::table('periodos')
                    ->select('periodos.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->paginate(6);              
        if( Auth::user()->nivel != '1' ){
        return redirect('/home');
        }     
        return view('layouts.periodos.periodos')->with('periodos',$periodos); 
    }

    public function store(Request $request)
    {        
        
        $validator = Validator::make($request->all(),[
            'nombre'=>'required',
            'fechaInicio'=>'required',
            'fechaFin' => 'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{
            
            $periodo = Periodo::create([
                'nombre'=>$request->nombre,
                'fecha_inicio'=> $request->fechaInicio,
                'fecha_fin'=>$request->fechaFin
            ]);
            //$idPeriodo = Periodo::latest('id')->first();    
            //\Session::put('periodo',$idPeriodo->nombre);
            return back()->with('Listo','Ingresado correctamente');
        
        }
        
    }

    public function editarPeriodo(Request $request)
    {
        
        $periodo = Periodo::find($request->id);
        $validator = Validator::make($request->all(),[
            'nombre'=>'required',
            'fechaInicio'=>'required',
            'fechaFin' => 'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{

              $periodo->nombre= $request->nombre;
              $periodo->fecha_inicio= $request->fechaInicio;
              $periodo->fecha_fin= $request->fechaFin;
              $periodo->save();
              return back()->with('Listo','Datos actualizado correctamente');
           
        }
    }

    public function getPeriodoDelete($id)
    {
        
        $periodo = Periodo::find($id);

        $registros=Curso::where('id_periodo',$id,)->whereNull('deleted_at')->get()->toArray();
        //Recorremos los cursos y eliminamos los cursos
        foreach($registros as $registro){
            $eliminados = Curso::destroy($registro);
        }
        $periodo->delete();
        session()->forget('idPeriodo');
        return back()->with('Listo','Registro eliminado exitosamente');
        
    }

}
