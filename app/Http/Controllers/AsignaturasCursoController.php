<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asignatura;
use App\AsignaturaCurso;
use App\Curso;
use App\Alumno;
use Validator;

class AsignaturasCursoController extends Controller
{

    public function __construct()
    {
        $this->middleware('preventBackHistory');
        $this->middleware('auth');
        
    }
    public function index($idCurso)
    {

        $idCursoRecibido = $idCurso;


        $asignaturas_cursos = \DB::table('asignaturas_cursos')
        ->select('id')
        ->whereNull('deleted_at')
        ->where('id_curso','=', $idCursoRecibido)
        ->where('id_periodo','=',\Session::get('idPeriodo'))
        ->get(); 

        $asignaturasCurso_count = count($asignaturas_cursos);
        
        if($asignaturasCurso_count > 0){
         
            $asignaturas = \DB::table('asignaturas')
            ->select('asignaturas.*')
            ->whereNull('deleted_at')
            ->orderBy('id','ASC')
            ->get();
            
        $asignaturasCurso = \DB::table('asignaturas_cursos')
        ->join('periodos','periodos.id' , '=' ,'asignaturas_cursos.id_periodo')
        ->join('cursos','cursos.id' , '=' ,'asignaturas_cursos.id_curso')
        ->join('asignaturas','asignaturas.id' , '=' ,'asignaturas_cursos.id_asignatura')
        ->select('asignaturas_cursos.id','asignaturas_cursos.id_asignatura','asignaturas_cursos.id_curso','cursos.nombre as nombreCurso', 'asignaturas.nombre')
        ->where('asignaturas_cursos.id_curso','=', $idCursoRecibido)
        ->where('periodos.id','=',\Session::get('idPeriodo'))
        ->whereNull('asignaturas_cursos.deleted_at')
        ->orderBy('asignaturas_cursos.id','ASC')
        ->get();

        return view("layouts.asignaturas.asignarAsignaturas", 
        ['asignaturasCurso' => $asignaturas_cursos,
        'asignaturas' => $asignaturas,  
        'idCursoRecibido' => $idCursoRecibido]);


        }
        else{
            $asignaturas = \DB::table('asignaturas')
                    ->select('asignaturas.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','ASC')
                    ->get();                    
        return view("layouts.asignaturas.asignarAsignaturas", 
        ['asignaturas' => $asignaturas, 
        'idCursoRecibido' => $idCursoRecibido]);
        }
        
    }

    public function store(Request $request)
    {
    $asignaturas = $request->asignaturas;
    $nombreCurso = $request->nombreCurso .' '.$request->nivel;
    $idPeriodo = (\Session::get('idPeriodo'));

    $messages = [
        'nombreCurso.unique' => 'El curso ingresado ya existe',

    ];
 /* Se concatena el nombre del curso */
    $request->merge([
        'nombreCurso' => $nombreCurso
    ]);
    /* Se valida que el nombre del curso no exista y en ese periodo*/
    $validator = Validator::make($request->all(),[
            'nivel' => 'required',
            'nombreCurso'=>'required|unique:cursos,nombre,NULL,id,deleted_at,NULL,id_periodo,'.$idPeriodo,
            
    ],$messages);

        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{

       $periodo = (\Session::get('idPeriodo'));
       $curso = Curso::create([
        'nombre'=>$nombreCurso,  
        'id_periodo'=>$periodo
    ]);
    return back()->with('Listo','Ingresado correctamente');       
    }
}


public function getCursoDelete($id)
{
    //Recuperamosel curso
    $curso = Curso::find($id);
    //Obtenemos los alumnos asociados  al curso
    $registros=Alumno::where('id_curso',$id,)->whereNull('deleted_at')->get()->toArray();
    //Recorremos los alumnos y eliminamos los alumnos
    foreach($registros as $registro){
        $eliminados = Alumno::destroy($registro);
    }
    //Eliminamos el curso
        $curso->delete();
        return redirect('/home')->with('Listo','Registro eliminado exitosamente');
    
}


public function actualizarAsignaturasCurso(Request $request)
{
    $idCurso = $request->idCursoActual;
    $idAsignatura = $request->asignaturasCurso;
    $periodo = (\Session::get('idPeriodo'));
    
    
    $asignaturas_cursos = \DB::table('asignaturas_cursos')
    ->select('id_asignatura')
    ->whereNull('deleted_at')
    ->where('id_curso','=', $idCurso);
    
    $asignaturas_cursos->delete();

    if(is_array($idAsignatura)){ 
    for ($i=0;$i<count($idAsignatura);$i++){
        

    $asignaturas_cursos = \DB::table('asignaturas_cursos')
    ->select('id_asignatura')
    ->whereNull('deleted_at')
    ->where('id_curso','=', $idCurso)
    ->where('id_asignatura',$idAsignatura[$i])
    ->where('id_periodo','=',\Session::get('idPeriodo'))
    ->get(); 

    $asignaturasCurso_count = count($asignaturas_cursos);
    
      if($asignaturasCurso_count == 0){

        $asignaturaCurso = AsignaturaCurso::create([
            'id_asignatura'=>$idAsignatura[$i],  
            'id_periodo'=>$periodo,
            'id_curso' => $idCurso
        ]);
    }
}
    }

    return back()->with('Listo','Ingresado correctamente');   



}
}
