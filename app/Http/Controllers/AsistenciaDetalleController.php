<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asistencia;
class AsistenciaDetalleController extends Controller
{

    public function __construct()
    {	
        $this->middleware('preventBackHistory');
        $this->middleware('auth');
        
    }

    public function index($idCurso,$fecha,$asignatura)
    {
        
        $alumnos = \DB::table('asistencias')
                    ->join('alumnos','alumnos.id' , '=' ,'asistencias.id_alumno')
                    ->join('cursos','cursos.id' , '=' ,'asistencias.id_curso')
                    ->join('asignaturas','asignaturas.id' , '=' ,'asistencias.id_asignatura')
                    ->select('alumnos.id','alumnos.nombre','alumnos.apellidos','alumnos.rut','asistencias.estado','asistencias.id as asistenciaId')
                    ->where('asistencias.fecha','=', $fecha)
                    ->where('asistencias.id_asignatura','=',$asignatura)
                    ->where('asistencias.id_curso','=',$idCurso)
                    ->whereNull('asistencias.deleted_at')
                    ->orderBy('asistencias.id','ASC')
                    ->get(); 

                    $asignaturaBuscada = \DB::table('asignaturas')
                    ->select('id')
                    ->whereNull('deleted_at')
                    ->where('id','=', $asignatura)
                    ->orderBy('id','DESC')
                    ->get();

                   
                    foreach($asignaturaBuscada as $a){
                        $nombreAsignatura = $a->id;
                    }
                    $asignaturas = \DB::table('asignaturas_cursos')
                    ->join('periodos','periodos.id' , '=' ,'asignaturas_cursos.id_periodo')
                    ->join('cursos','cursos.id' , '=' ,'asignaturas_cursos.id_curso')
                    ->join('asignaturas','asignaturas.id' , '=' ,'asignaturas_cursos.id_asignatura')
                    ->select('asignaturas_cursos.id_asignatura', 'asignaturas.nombre')
                    ->where('asignaturas_cursos.id_curso','=', $idCurso)
                    ->where('periodos.id','=',\Session::get('idPeriodo'))
                    ->whereNull('asignaturas_cursos.deleted_at')
                    ->orderBy('asignaturas_cursos.id','ASC')
                    ->get();
                    
                   

                    return view("layouts.asistencias.asistenciaDetalle", 
                    ['alumnos' => $alumnos, 
                    'idCursoActual' => $idCurso,
                    'asignaturas'=> $asignaturas,
                    'fechaBuscada' => $fecha,
                    'asignaturaBuscada' => $asignatura]); 
    }

    public function enviarAsistencia(Request $request, $idCurso){

  
        \Session::put('idCurso',$idCurso);
      
    $asistenciaId = $request->asistencia;
    dd($asistenciaId);
      foreach($asistenciaId as $idAsistencia => $e) {
          
          $asistencia = Asistencia::find($idAsistencia); 
          $asistencia->estado = $e;
          $asistencia->save();
          
      }
    return back()->with('Listo','Asistencia ingresada correctamente');
    }
}
