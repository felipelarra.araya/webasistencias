<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Periodo;

class IndexController extends Controller
{
    public function setPeriodo($id)
    {
    	\Session::put('idPeriodo',$id);
        $periodo = Periodo::select('nombre')->where('id',$id)->first();
        return back()->with('periodo',$periodo->nombre);
    }
}
