<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asignatura;
use Validator;
use Auth;
use Illuminate\Support\Facades\Session;

class AsignaturasController extends Controller
{
    public function __construct()
    {	
        $this->middleware('preventBackHistory');
        $this->middleware('auth');
        
    }
    public function index()
    {
        $asignaturas = \DB::table('asignaturas')
                    ->select('asignaturas.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->paginate(6);   
                    if( Auth::user()->nivel != '1' ){
                        return redirect('/home');
                        }               
        return view('layouts.asignaturas.asignaturas')->with('asignaturas',$asignaturas); 
    }

  

    public function store(Request $request)
    {        
        $messages = [
            'nombre.unique' => 'La Asignatura ya existe',
    
        ];
        
        $validator = Validator::make($request->all(),[
            //'nombre'=>'required|unique:asignaturas,nombre,null,id,deleted_at,null'
            'nombre'=>'required|unique:asignaturas,nombre,NULL,id,deleted_at,NULL'
        ],$messages);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{
            
            $asignatura = Asignatura::create([
                'nombre'=>$request->nombre

            ]);
            //return back()->with('Listo','Ingresado correctamente');
            //flash('Listo','Ingresado correctamente')->success();
            Session::flash('Listo', 'Ingresado correctamente');
            return redirect()->route('asignaturas');
        
        }
        
    }


    
    public function editarAsignatura(Request $request)
    {
        
        $asignatura = Asignatura::find($request->id);
        $validator = Validator::make($request->all(),[
            'nombre'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{

            
              $asignatura->nombre= $request->nombre;
              $asignatura->save();
              return back()->with('Listo','Datos actualizado correctamente');
           
        }
    }


    public function getAsignaturaDelete($id)
    {
        
        $asignatura = Asignatura::find($id);
        if($asignatura->delete()){
            return back()->with('Listo','Registro eliminado exitosamente');
        }
    }
}
