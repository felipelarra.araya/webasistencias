<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Periodo extends Model
{
    use SoftDeletes; 
    protected $fillable = ['nombre','fecha_inicio','fecha_fin'];
    protected $table = "periodos";
    protected $dates = ['deleted_at'];
    protected $hidden =   ['created_at','updated_at'];
}
