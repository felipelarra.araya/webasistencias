<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Asistencia extends Model
{
    use SoftDeletes; 
    protected $fillable = ['estado','id_curso','id_asignatura','id_alumno','fecha'];
    protected $table = "asistencias";
    protected $dates = ['deleted_at'];
    protected $hidden =   ['created_at','updated_at'];
}
