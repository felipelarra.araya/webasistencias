<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AsignaturaCurso extends Model
{
    use SoftDeletes; 
    protected $fillable = ['id_asignatura','id_periodo','id_curso'];
    protected $table = "asignaturas_cursos";
    protected $dates = ['deleted_at'];
    protected $hidden =   ['created_at','updated_at'];
}
