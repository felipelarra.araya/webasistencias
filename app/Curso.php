<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Curso extends Model
{
    use SoftDeletes; 
    protected $fillable = ['nombre','id_periodo'];
    protected $table = "cursos";
    protected $dates = ['deleted_at'];
    protected $hidden =   ['created_at','updated_at'];
}
