<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asignatura extends Model
{
    use SoftDeletes; 
    protected $fillable = ['nombre'];
    protected $table = "asignaturas";
    protected $dates = ['deleted_at'];
    protected $hidden =   ['created_at','updated_at'];
}
