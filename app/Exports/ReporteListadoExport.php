<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ReporteListadoExport implements WithMultipleSheets
{
    
    private $data;
    private $idAsignaturas;
    private $countAsignaturas;
    private $fechaInicio; 
    private $idCurso;
    private $fechaFin;

    public function __construct($idAsignaturas,$countAsignaturas,$fechaInicio,$idCurso,$fechaFin)
    {
        $this->idAsignaturas = $idAsignaturas;
        $this->fechaInicio = $fechaInicio; 
        $this->idCurso  = $idCurso; 
        $this->fechaFin = $fechaFin;
        $this->countAsignaturas = $countAsignaturas;
        return $this;
    }


    public function sheets(): array
    {
        
        foreach($this->idAsignaturas as $ids){
            $idAsigna[] = $ids;
        }
        
       $sheets = [];
       foreach(range(1,$this->countAsignaturas) as $countAsignatura => $i){
           
           $sheets[] = new ReporteExport($this->fechaInicio,$this->idCurso,$idAsigna[$countAsignatura],$this->fechaFin);
       }
       
        return $sheets;
    }
    

}
