<?php

namespace App\Exports;

use App\Asistencia;

use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Contracts\View\View;

class ReporteExport implements FromView,WithTitle
{

    private $fechaInicio; 
    private $idCurso;
    private $fechaFin;
    private $idAsignatura;
    
    public function __construct(string $fechaInicio,int $idCurso, int $idAsignatura, string $fechaFin) 
    {
        $this->fechaInicio = $fechaInicio; 
        $this->idCurso  = $idCurso; 
        $this->fechaFin = $fechaFin;
        $this->idAsignatura = $idAsignatura;
    }    
    public function view(): View
    {
        
        $rango = 0;
        $rango = ((strtotime($this->fechaFin)-strtotime($this->fechaInicio))+(24*60*60)) /(24*60*60);
       

        $alumnos = \DB::table('alumnos')
                    ->select('alumnos.id','alumnos.nombre' , 'alumnos.apellidos')
                    ->whereNull('deleted_at')
                    ->where('id_curso','=', $this->idCurso)
                    ->orderBy('id','DESC')
                    ->get();
        
                    
        return view('layouts.reportes.reporteExcel', [
            
            'alumnos' => $alumnos,
            'rango' => $rango,
            'idCursoActual' => $this->idCurso,
            'idAsignatura' => $this->idAsignatura,
            'fechaInicio' => $this->fechaInicio
        ]);
    }

    public function title(): string
    {
        $nombre = \DB::table('asignaturas')
                    ->whereNull('deleted_at')
                    ->where('id','=', $this->idAsignatura)
                    ->pluck('nombre');
                    
        return $nombre;
    }
}
