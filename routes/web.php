<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.main');
});



 /*----------------------Home Cursos-------------*/

 Route::get('/home', 'HomeController@index');
 



 /*---------------- Alumnos-------------*/
 Route::get('/alumnos/add/{idCurso}', 'AlumnosController@getAlumnosAdd');
 Route::get('/alumnos/{id}', 'AlumnosController@index')->name('alumnos');
 Route::post('/alumnos/edit', 'AlumnosController@editarAlumno');
 Route::get('/alumnos/{id}/delete', 'AlumnosController@getAlumnoDelete');
 Route::post('/alumnos/subirExcel', 'AlumnosController@subirExcel')->name('subirExcel');
 Route::get('/alumnosDescarga/descargarExcel', 'AlumnosController@descargarExcel')->name('descargarExcel');
 Route::resource('alumnos','AlumnosController');

 
 /*---------------- Asignaturas-------------*/
 Route::get('/asignaturas', 'AsignaturasController@index')->name('asignaturas');
 Route::get('/asignaturas/{id}/delete', 'AsignaturasController@getAsignaturaDelete');
 Route::post('/asignaturas/edit', 'AsignaturasController@editarAsignatura');
 Route::resource('asignaturas','AsignaturasController');

  /*Asignar asignaturas a los cursos*/
  Route::get('/asignaturasCurso/inicio/{idCurso}', 'AsignaturasCursoController@index');
  Route::get('/asignaturasCurso/{id}/delete', 'AsignaturasCursoController@getCursoDelete');
  Route::post('/asignaturasCurso/modificarAsignaturasCurso', 'AsignaturasCursoController@actualizarAsignaturasCurso');
  Route::resource('asignaturasCurso','AsignaturasCursoController');

  /*Asistencias*/
  Route::get('/asistencias/{idCurso}', ["as" => "index", "uses" => "AsistenciasController@index"]); 
  Route::get('/asistencias/asistenciaDetalle/{idCurso}/{fecha}/{asignatura}', ["as" => "detalleAsistencia", "uses" => "AsistenciaDetalleController@index"]);
  Route::post('/asistencias/enviar/{idCurso}', 'AsistenciasController@enviarAsistencia');
  Route::post('/asistencias/buscar', 'AsistenciasController@buscarAsistencia')->name('buscarAsistencia');
  Route::resource('asistencias','AsistenciasController');

  /* Reporte  */
  Route::get('/reporte/{idCurso}', 'ReportesController@index');
  Route::post('/reporte/buscar', ['as' => 'buscarReporte' , 'uses' => 'ReportesController@buscar']);
  Route::resource('reporte','ReportesController');

  Route::get('/reporte/exportarTodo{fechaInicio}/{idAsignatura}/{idCurso}/{fechaFin}', 'ReportePdfController@ExportarTodoExcel')->name('exportarTodo');
  Route::get('/reporte/exportar{fechaInicio}/{idAsignatura}/{idCurso}/{fechaFin}', 'ReportePdfController@ExportarExcel')->name('exportar');
  Route::resource('reporte','ReportePdfController');

   
 /*---------------- Asignaturas-------------*/
 Route::get('/periodos', 'PeriodosController@index');
 Route::get('/periodos/{id}/delete', 'PeriodosController@getPeriodoDelete');
 Route::post('/periodos/edit', 'PeriodosController@editarPeriodo');
 Route::resource('periodos','PeriodosController');

 Route::get('/cambiarContraseña', 'UserController@index');
 Route::post('/cambiarContraseña/edit', 'UserController@cambiarContraseña');
 Route::post('/registrarUsuario', 'UserController@ingresarUsuario')->name("ingresarUsuario");
 
 Route::resource('User','UserController');




Route::get('/setperiodo/{id}','IndexController@setPeriodo');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
