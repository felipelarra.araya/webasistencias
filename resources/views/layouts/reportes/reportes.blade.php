@extends('layouts.main')
@section('contenido')  
<div class="col-md-12 mt-2">
<a  href="{{ route('alumnos',['idCurso'=> $idCursoActual]) }}"><i class='fa fa-chevron-left '></i> Regresar</a>
</div>
<div class="container mt-2">
<div class="alert alert-primary mt-1" role="alert">Reporte Asistencia</div> 


<form onsubmit="return false" id="formulario_busqueda">
@csrf

<div class="form-group ">
<input type="date" name="fechaInicio" value="<?php echo date("Y-m-d");?>" required class="form-control">
</div>
<div class="form-group ">
<input type="date" name="fechaFin" value="<?php echo date("Y-m-d");?>" required class="form-control">
</div>

<div class="form-group">
<select class="form-control" required name="asignatura" id="asignatura">
<option value="">Elige una asignatura</option>
@foreach($asignaturas as $asignatura)
           
  <option value="{{$asignatura->id_asignatura}}">{{$asignatura->nombre}}</option>
           
@endforeach
</select>  
</div>

<button class="btn btn-primary tamañoBoton" name="buscarAsistencia" value="buscar"  type="submit">Buscar</button>

<input type="hidden" value="{{$idCursoActual}}" name="idCursoActual">

</form>
<div id="table-body">
</div>


</div>

    

@endsection

@section('scripts')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<script>
$("#formulario_busqueda").submit(function() {
  
  var dataString = $('#formulario_busqueda').serialize();
             $.ajax({
                url: '/reporte/buscar',
                type: 'POST',
                data: dataString,
                dataType: 'JSON',
                beforeSend: function() {
                  
                  $('#table-body').html("<img src='/images/loader.gif' />");
        },
                success: function (response) {
               $("#table-body").html(response);
              }
             });
            
          });
        </script>

        
@endsection