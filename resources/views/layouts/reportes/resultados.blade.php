
<?php  $alums = $alumnos; ?>
<a  href="{{ route('exportar', ['fechaInicio' => $fechaInicio, 'idAsignatura' => $idAsignatura,'idCurso' => $idCursoActual , 'fechaFin' => $fechaFin]) }}"> <button class="btn btn-outline-secondary mt-2 tamañoBoton"  type="button">Exportar</button></a>
<a  href="{{ route('exportarTodo', ['fechaInicio' => $fechaInicio, 'idAsignatura' => $idAsignatura,'idCurso' => $idCursoActual , 'fechaFin' => $fechaFin]) }}"><button class="btn btn-outline-info mt-2 tamañoBoton"  type="button">Exportar Todo</button></a>
 
<div class="table-wrapperAsistencia mt-4">  
<table class="table table-bordered table-hover mt-5 table-responsive-lg">

<thead class="thead">
    
      <th>Alumno</th>
      @for($i=0; $i < $rango;$i++)
      <th style="font-size: 0.80rem;"><?php setlocale(LC_TIME, "spanish"); echo strftime("%d-%b", strtotime($fechaInicio)+($i*(24*60*60)));?></th>
    @endfor
  </thead>
  <tbody>
  @foreach($alums as $al)
  <?php
  $idAlumno = $al->id;
  
  ?>  
  <tr>
    <td ><?php echo $al->nombre." ".$al->apellidos; ?></td>
      @for($i=0; $i < $rango;$i++)
      <?php
      $date_at= date("Y-m-d",strtotime($fechaInicio)+($i*(24*60*60)));

      $asist = App\Asistencia::selectRaw('asistencias.estado')
      ->join('asignaturas','asignaturas.id' , '=' ,'asistencias.id_asignatura')
      ->join('alumnos','alumnos.id' , '=' ,'asistencias.id_alumno')
      ->where('asistencias.fecha','=', $date_at)
      ->where('asistencias.id_curso','=', $idCursoActual)
      ->where('asistencias.id_asignatura','=', $idAsignatura)
      ->where('asistencias.id_alumno','=', $idAlumno)
      ->whereNull('asistencias.deleted_at')
      ->first();
      
      ?>
 
  
 
  
      <td>
      @if($asist["estado"] != null)
        
      @if($asist["estado"] == 1 )
      <i style="font-size: 0.80rem;" class='fa fa-check'></i>
      @endif
      @if($asist["estado"] == 2 )
      <i style="font-size: 0.80rem;" class='fa fa-times'></i>
      @endif  
      @endif
      
      </td>
      
      @endfor
    </tr>
  </tbody>
  @endforeach
  
</table>
<div class="col-sm-12">
<div id="piechart"</div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Asistencia', 'Numero'],
         
           @foreach($asisten as $asistencia)
           ['{{ $asistencia->estado_asistencia }}',{{ $asistencia->presente_ausente }}],

          @endforeach

        ]);

        var options = {
          title: 'Porcentaje de asistencia de alumnos',
          slices: {0: {color: '#666666'}, 1:{color: '#006EFF'}}
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }

</script>