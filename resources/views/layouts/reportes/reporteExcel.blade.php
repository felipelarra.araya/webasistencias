<?php  $alums = $alumnos; ?>
<table class="table table-bordered table-hover mt-5 text-center">
  <thead>
  <tr>
      <th scope="col">Nombre</th>
      @for($i=0; $i < $rango;$i++)
      <th><?php  echo date("d-M",strtotime($fechaInicio)+($i*(24*60*60)));?></th>
    @endfor
    
  </tr>
  </thead>
  <tbody>
  @foreach($alums as $al)
  <?php
  $idAlumno = $al->id;
  
  ?>  
  <tr>
    <td class="mt-5"><?php echo $al->nombre." ".$al->apellidos; ?></td>
      @for($i=0; $i < $rango;$i++)
      <?php
      $date_at= date("Y-m-d",strtotime($fechaInicio)+($i*(24*60*60)));

      $asist = App\Asistencia::selectRaw('asistencias.estado')
      ->join('asignaturas','asignaturas.id' , '=' ,'asistencias.id_asignatura')
      ->join('alumnos','alumnos.id' , '=' ,'asistencias.id_alumno')
      ->where('asistencias.fecha','=', $date_at)
      ->where('asistencias.id_curso','=', $idCursoActual)
      ->where('asistencias.id_asignatura','=', $idAsignatura)
      ->where('asistencias.id_alumno','=', $idAlumno)
      ->whereNull('asistencias.deleted_at')
      ->first();
      

      
      ?>

      <td>
      @if($asist["estado"] != null)
        
      @if($asist["estado"] == 1 )
      {{'Presente'}}
      @endif
      @if($asist["estado"] == 2 )
      {{'Ausente'}}
      @endif  
      @endif
      
      </td>
      
      @endfor
    </tr>
    @endforeach
  </tbody>
 
  
</table>

