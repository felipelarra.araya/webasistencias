@extends('layouts.main')
@section('contenido')  

<!--
<div class="col-md-12 mt-2">
<a href="{{ route('alumnos',['idCurso'=> $idCursoActual]) }}"><button type="button" class="btn btn-outline-primary float-right"><i class='fa fa-chevron-left'></i> Regresar</button></a>
</div>-->
<div class="container mt-2">
<div class="alert alert-primary mt-1" role="alert">Reporte Asistencia</div> 
<form  method="post" action="{{ url('/reporte/buscar')}}">
@csrf

<div class="form-group">
<input type="date" name="fechaInicio" value="<?php echo $fechaInicio;?>" required class="form-control">
</div>
<div class="form-group ">
<input type="date" name="fechaFin" value="<?php echo $fechaFin;?>" required class="form-control">
</div>
<div class="form-group">
<select class="form-control" name="asignatura" id="asignatura">


@foreach($asignaturas as $asignatura)
        @if($asignatura->id_asignatura == $asignaturaBuscada->id)
            <option value="{{$asignatura->id_asignatura}}" selected>{{$asignatura->nombre}}</option>
        @else
            <option value="{{$asignatura->id_asignatura}}">{{$asignatura->nombre}}</option>
        @endif
    @endforeach


</select>  
</div>

<button class="btn btn-outline-primary" name="buscarAsistencia" value="buscar" type="submit">Buscar</button>
<input type="hidden" value="{{$idCursoActual}}" name="idCursoActual">


<?php  $alums = $alumnos; ?>
<a  href="{{ route('exportar', ['fechaInicio' => $fechaInicio, 'idAsignatura' => $idAsignatura,'idCurso' => $idCursoActual , 'fechaFin' => $fechaFin]) }}"> <button class="btn btn-outline-secondary" type="button">Exportar</button></a>
<a class="px-2" href="{{ route('exportarTodo', ['fechaInicio' => $fechaInicio, 'idAsignatura' => $idAsignatura,'idCurso' => $idCursoActual , 'fechaFin' => $fechaFin]) }}"><button class="btn btn-outline-info" type="button">Exportar Todo</button></a>
<div class="table-wrapper">  
<table class="table table-bordered table-hover mt-5 table-responsive-lg">

  <thead>
    
      <th>Alumno</th>
      @for($i=0; $i < $rango;$i++)
      <th style="font-size: 0.80rem;"><?php setlocale(LC_TIME, "spanish"); echo strftime("%d-%b", strtotime($fechaInicio)+($i*(24*60*60)));?></th>
    @endfor
  </thead>
  <tbody>
  @foreach($alums as $al)
  <?php
  $idAlumno = $al->id;
  
  ?>  
  <tr>
    <td style="font-size: 0.80rem;" ><?php echo $al->nombre." ".$al->apellidos; ?></td>
      @for($i=0; $i < $rango;$i++)
      <?php
      $date_at= date("Y-m-d",strtotime($fechaInicio)+($i*(24*60*60)));

      $asist = App\Asistencia::selectRaw('asistencias.estado')
      ->join('asignaturas','asignaturas.id' , '=' ,'asistencias.id_asignatura')
      ->join('alumnos','alumnos.id' , '=' ,'asistencias.id_alumno')
      ->where('asistencias.fecha','=', $date_at)
      ->where('asistencias.id_curso','=', $idCursoActual)
      ->where('asistencias.id_asignatura','=', $idAsignatura)
      ->where('asistencias.id_alumno','=', $idAlumno)
      ->whereNull('asistencias.deleted_at')
      ->first();

   

    

          $asisten = DB::table('asistencias')
        ->selectRaw('count(*) as presente_ausente, estado, case
                when estado = 1 then "Presente"
                when estado = 2 then "Ausente"
                end as estado_asistencia')

        ->where('id_curso', '=', $idCursoActual)
        ->where('id_asignatura', '=', $idAsignatura)
        ->whereBetween('fecha', [$fechaInicio, $fechaFin])
        ->groupBy('estado')
        ->get();
      
      


                  
      
      ?>
 
  
 
  
      <td>
      @if($asist["estado"] != null)
        
      @if($asist["estado"] == 1 )
      <i style="font-size: 0.80rem;" class='fa fa-check'></i>
      @endif
      @if($asist["estado"] == 2 )
      <i style="font-size: 0.80rem;" class='fa fa-times'></i>
      @endif  
      @endif
      
      </td>
      
      @endfor
    </tr>
  </tbody>
  @endforeach
  
</table>
</form>
</div>

<div class="col-md-12">
<div id="piechart" style="width: 900px; height: 500px;"></div>
</div>
</div>

 




@endsection

@section('scripts')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Asistencia', 'Numero'],
         
           @foreach($asisten as $asistencia)
           ['{{ $asistencia->estado_asistencia }}',{{ $asistencia->presente_ausente }}],

          @endforeach

        ]);

        var options = {
          title: 'Porcentaje de asistencia de alumnos',
          slices: {0: {color: '#666666'}, 1:{color: '#006EFF'}}
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }


      
    </script>
@endsection