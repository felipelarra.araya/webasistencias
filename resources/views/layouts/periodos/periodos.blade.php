@extends('layouts.main')
@section('contenido')

    <div class="container mt-4">
 
   
<div class="alert alert-primary" role="alert">
  Periodos
</div>

    <button type="button"  data-toggle="modal" data-target="#modalperiodo" class="btn btn-outline-primary"><i class='fa fa-plus'></i> Nuevo periodo</button>      
    
    <table class="table col-12 table-bordered table-hover mt-2">
      <thead>
      
      @if(count($periodos)>0)
        <tr>
          
          <th class="table-primary" scope="col">Nombre</th>
          <th class="table-primary" scope="col">Fecha Inicio</th>
          <th class="table-primary" scope="col">Fecha Fin</th>
          <th class="table-primary" scope="col">Acciones</th>
          
        </tr>
      </thead>
      <tbody>
      @foreach($periodos as $periodo)
        <tr>
          <td >{{ $periodo->nombre }}</td>
          <td>{{ \Carbon\Carbon::parse($periodo->fecha_inicio)->format('d/m/Y')}}</td>
          <td>{{ \Carbon\Carbon::parse($periodo->fecha_fin)->format('d/m/Y')}}</td>
        </a>
          <td>
          <button type="button"class="btn btn-round btnEditar"data-toggle="modal" data-target="#modalEditar{{$periodo->id}}"><i class="fa fa-edit"></i>
          </button>
          <button class="btn btn-round btnEliminar" data-toggle="modal" data-target="#modalEliminar{{$periodo->id}}"><i class="fa fa-trash"></i> 
          </button>
          </td>

          
  <!-- Modal Eliminar -->
<div class="modal fade" id="modalEliminar{{$periodo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Eliminar periodo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     

        <div class="modal-body">
        <h5 class="col-11 text-center">¿Desea eliminar el periodo?</h5>
        
        </div>
           
        <div class="modal-footer">
          <a type="button"  class="btn btn-danger tamañoBoton" href="{{ url('/periodos/'.$periodo->id.'/delete')}}">Si</a> 
          <button type="button"  class="btn btn-secondary tamañoBoton" data-dismiss="modal">No</button>
        </div>
 
    </div>
  </div>
</div>
<!-- Modal editar-->
<div class="modal fade" id="modalEditar{{$periodo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Modificar periodo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
    <div class="form-row">
    <div class="col-md-12 mb-3">
    <form action="/periodos/edit" method="post">
      @csrf
    <label>Nombre</label>
      <input type="text" name="nombre" class="form-control" placeholder="Nombre periodo" value="{{ $periodo->nombre }}"  required>
    <label>Fecha Inicio</label>
      <input type="date" name="fechaInicio" value="{{$periodo->fecha_inicio}}" required class="form-control"> 
    <label>Fecha Fin</label>
      <input type="date" name="fechaFin" value="{{$periodo->fecha_fin}}" required class="form-control"> 
    </div>
</div>
</div>
        <div class="modal-footer">
         <!-- Mandamos el id de la periodo a editar -->   
        <input type="hidden" name="id" id="idEdit" value="{{$periodo->id}}">
        <button class="btn btn-primary tamañoBoton"  type="submit">Guardar</button>   
        <button type="button" class="btn btn-secondary tamañoBoton" data-dismiss="modal">Cerrar</button> 
        
        </div>
        </form>
    </div>
  </div>
</div>
          </tr>
        
        @endforeach
        </tbody>
            </table>
            {{$periodos->links()}}
@else
<div class="mt-1 col-md-4  text-center"> 
<h4>{{"No existen Registros"}}</h4>
</div>

@endif

</div>



<div class="modal fade" id="modalperiodo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Agregar periodo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
    <div class="form-row">
    <div class="col-md-12 mb-3">
    <form action="/periodos" method="post">
      @csrf
    <label>Nombre</label>
      <input type="text" name="nombre" class="form-control" placeholder="Nombre periodo"  required>
      <label>Fecha Inicio</label>
      <input type="date" name="fechaInicio"value="<?php echo date("Y-m-d");?>"required class="form-control"> 
    <label>Fecha Fin</label>
      <input type="date" name="fechaFin"value="<?php echo date("Y-m-d");?>"required class="form-control">
    </div>
</div>
</div>
        <div class="modal-footer">
        <button class="btn btn-primary" type="submit">Guardar</button>   
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button> 
        
        </div>
        </form>
    </div>
  </div>
</div>

@endsection


@section('scripts')


@endsection