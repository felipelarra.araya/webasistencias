<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Administración - Santa María de Belén</title>
    
        <link href="{{ asset('images/logoSinFondo.png')}}" rel="icon">
        <link href="{{ asset('/css/styles.css') }}" rel="stylesheet">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        @yield('css')
    </head>
    <body class="sb-nav-fixed">
    @include('layouts.header')

        <div id="layoutSidenav">
        @include('layouts.sidebar')
            <div id="layoutSidenav_content">
            
            @yield('contenido')
            
            
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy;Santa María de Belén 2020</div>
                           
                        </div>
                    </div>
                </footer>
            </div>
        </div>
      
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('/js/scripts.js')}}"></script> 
        <script src="{{ asset('/js/sweetAlert.js')}}"></script> 

        <script>
$(document).ready(function() {

    
    
  @if(Session::get('Listo'))
  //alert('{{ session('status') }}');
  Swal.fire({
  position: 'center',
  icon: 'success',
  title: '{{ session('Listo') }}',
  showConfirmButton: false,
  timer: 1500
});      
  @endif

  @if(Session::get('ErrorInsert'))
  @foreach($errors->all() as $error)
  
  
  Swal.fire({
  position: 'center',
  icon: 'error',
  title: '{{ $error }}',
  showConfirmButton: true,
  
});      
@endforeach
  @endif
   
  
  @if(Session::get('ErrorContraseña'))
  
  Swal.fire({
  position: 'center',
  icon: 'error',
  title: '{{ session('ErrorContraseña') }}',
  showConfirmButton: true,
  
});      
  @endif

  @if(Session::get('ErrorRegistro'))
  //alert('{{ session('status') }}');
  Swal.fire({
  position: 'center',
  icon: 'error',
  title: '{{ session('ErrorRegistro') }}',
  showConfirmButton: false,
  timer: 1500
});      
  @endif

});
</script>
        
        @yield('scripts')
    </body>
</html>
