<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="{{Route('home')}}">Asistencia SMB</a>
            <button class="btn btn-link btn-sm order-1 order-lg-0 d-block d-sm-none" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="mt-4 d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                <div class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">  
                <span id="NombrePeriodo" style="font-weight: bold;">
                  @if(\Session::has('periodo'))
                    {{ \Session::get('periodo') }}
                  @else
                    <?php
                      $defecto = App\Periodo::select('nombre')->where('id',
                        \Session::get('idPeriodo'))->first();
                    ?>
                    
                      @if(is_null($defecto))
                        Seleccione Periodo
                      @else
                        {{ $defecto->nombre }}
                      @endif
                  @endif
            </span>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu" id="Periodos">
                @foreach(App\Periodo::orderBy('id','DESC')->get() as $periodo)
                  <li>
             
                  <a class="px-2" href="{{ url('setperiodo/'.$periodo->id)}}">
                      {{ $periodo->nombre }}
                    </a>
                  </li>
                @endforeach

              </ul>
          </div>

                </div>
            </form>

            
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i>
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"> {{ Auth::user()->name }}</span>
                    </a>
                    
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="/cambiarContraseña">Cambiar Contraseña</a>
                        @if(Auth::user()->nivel == '1')
                        <a class="dropdown-item" href="{{ route('register') }}">Agregar Usuario</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Salir
                </a>
                    </div>
                </li>
            </ul>
        </nav>

         <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Cerrar Sesión?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body col-11 text-center">¿Desea cerrar sesión?</div>
        <div class="modal-footer">
          
          <form id="logout-form" action="{{ route('logout') }}" method="POST">
          @csrf
          <button class="btn btn-primary"  type="submit">Salir</button>
          <button class="btn btn-secondary"  type="button" data-dismiss="modal">Cancelar</button>
         </form>
          
        </div>
      </div>
    </div>
  </div>
