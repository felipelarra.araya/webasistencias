@extends('layouts.main')
@section('contenido')  


        <div class="container">
                <div class="alert alert-primary mt-1" role="alert">Asistencia</div> 
      

<form class="form-inline" method="post" action="{{ url('/asistencias/buscar')}}">
@csrf
<div class="form-group">
<input type="date" name="fecha" value="<?php echo $fechaBuscada;?>" required class="form-control" >
   </div>
   <div class="form-group">
   <select class="form-control" name="asignatura" id="asignatura">



        @foreach($asignaturas as $asignatura)
        @if($asignatura->id_asignatura == $asignaturaBuscada)
            <option value="{{$asignatura->id_asignatura}}" selected>{{$asignatura->nombre}}</option>
        @else
            <option value="{{$asignatura->id_asignatura}}">{{$asignatura->nombre}}</option>
        @endif
    @endforeach
</select>  
 </div>
 <input type="hidden" value="{{$idCursoActual}}" name="idCursoActual">
 <div class="row">
 <div class="px-3">
<button class="btn btn-primary" name="buscarAsistencia"  type="submit">Buscar</button>
</div>

</div>

<div class="col-md-12">
<a href="{{ route('alumnos',['idCurso'=> $idCursoActual]) }}"><button type="button" class="btn btn-outline-primary float-right"><i class='fa fa-chevron-left'></i> Regresar</button></a>
</div>
</form>


<form class="form-inline" method="post" action="{{ url('/asistenciasDetalle/enviar/'.$idCursoActual)}}">
@csrf
 <table class="table col-12 table-bordered table-hover mt-2">
      <thead>
        <tr>
          <th class="text-center" scope="col">#</th>
          <th class="text-center" scope="col">Nombre</th>
          <th class="text-center" scope="col">Apellidos</th>
          <th class="text-center" scope="col">Rut</th>
          <th class="text-center" scope="col"><button type="button" class="btn btn-link check-all">Seleccionar todos</button></th>
          
        </tr>
      </thead>
      <tbody>
      @foreach($alumnos as $index => $alumno)
        <tr>
          <td>{{ $index +1}}</td>
          <td>{{ $alumno->nombre }}</td>
          <td>{{ $alumno->apellidos}}</td>
          <td>{{ $alumno->rut}}</td>
        
          <td>
          <div class="form-check">

          <input type="hidden" name="asistencia[{{$alumno->asistenciaId}}]" value="2">  
          <input type="checkbox"  name="asistencia[{{$alumno->asistenciaId}}]" value="1"<?php echo ($alumno->estado == 1 ? ' checked' : '')?> class="form-check-input settings" id="checkAsistencia">
          

      
  </div>
          </td>
          </tr>
        
        @endforeach
        </tbody>
            </table>
     <div class="col-md-12">        
  <button type="submit" value="Enviar" name="btnEnviar" class="btn btn-primary float-right">Enviar Asistencia</button>
</div>
        </form>

</div>

@endsection

@section('scripts')
<script>


var checked = false;

$('.check-all').on('click',function(){

if(checked == false) {
$('.settings').prop('checked', true);

checked = true;
} else {
$('.settings').prop('checked', false);

checked = false;
}

});







</script>

@endsection