
<div class="table-wrapperAsistencia mt-4">  
<table class="table col-12 table-bordered table-hover mt-2">
        <thead class="thead">
        <tr>
          <th class="text-center" scope="col">#</th>
          <th class="text-center" scope="col">Nombre</th>
          <th class="text-center" scope="col">Apellidos</th>
          <th class="text-center" scope="col">Rut</th>
          <th class="text-center" scope="col"><button type="button" style="color:white;"  class="btn btn-link check-all">Seleccionar todos</button></th>
          
        </tr>
      </thead>
      <tbody>
@foreach($data as $index => $alumno)
        <tr>
          <td>{{ $index +1}}</td>
          <td>{{ $alumno->nombre }}</td>
          <td>{{ $alumno->apellidos}}</td>
          <td>{{ $alumno->rut}}</td>

          <td>
          <div class="form-check">

          <input type="hidden" name="asistencia[{{$alumno->id}}]" value="2">  
          <input type="hidden" name="opcion" value="registrar">  
          <input type="hidden" name="fecha" value="{{$fecha}}">  
          <input type="hidden" name="asignatura" value="{{$asignatura}}"> 
          <input type="checkbox" checked name="asistencia[{{$alumno->id}}]" value="1" class="form-check-input settings " id="checkAsistencia">
          

      
  </div>
          </td>
          </tr>
        
        @endforeach
        </tbody>
            </table>
</div>
 <div class="col-md-12 col-sm-12 mt-2">        
  <button type="submit" value="Enviar" name="btnEnviar" class="btn btn-primary float-right">Enviar Asistencia</button>
</div>
        
</div>

<script>
var checked = false;

$('.check-all').on('click',function(){

if(checked == false) {
$('.settings').prop('checked', true);


checked = true;
} else {
$('.settings').prop('checked', false);

checked = false;
}

});





</script>
    