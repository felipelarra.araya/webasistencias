@extends('layouts.main')
@section('contenido')  

<div class="col-md-12 mt-2">
<a class="float-left" href="{{ route('alumnos',['idCurso'=> $idCursoActual]) }}"><i class='fa fa-chevron-left '></i> Regresar</a>
</div>
                <div class="container mt-2">    
                <div class="alert alert-primary mt-1" role="alert">Asistencia</div> 
     

<form   onsubmit="return false" id="formulario_busqueda">
@csrf



<div class="form-group">
<input type="date" id="fecha" name="fecha" value="<?php echo date("Y-m-d");?>" required class="form-control">
</div>
  <div class="form-group">
   <select class="form-control" required name="asignatura" id="asignatura">
   <option value="">Elige una asignatura</option>
@foreach($asignaturas as $asignatura)
           
           <option value="{{$asignatura->id_asignatura}}">{{$asignatura->nombre}}</option>
           
           @endforeach

</select>
</div>
<div class="px-1">
<input type="hidden" value="{{$idCursoActual}}" name="idCursoActual">
<button class="btn btn-primary" name="buscarAsistencia"  type="submit">Buscar</button>
</div>  
 </div>
</form>
<form  method="post" action="{{ url('/asistencias/enviar/'.$idCursoActual)}}">
@csrf

<div id="table-body">
</div>
</form>

@endsection

@section('scripts')
<script>


$("#formulario_busqueda").submit(function() {
  var dataString = $('#formulario_busqueda').serialize();
             $.ajax({
                url: '/asistencias/buscar',
                type: 'POST',
                data: dataString,
                dataType: 'JSON',
                beforeSend: function() {
                  $('#table-body').html("<img src='/images/loader.gif' />");
        },
                success: function (response) {
               $("#table-body").html(response);
              }
             });
            
          });
          
          /*

           $("#formulario_enviar").submit(function() {
  var dataString = $('#formulario_enviar').serialize();
             $.ajax({
                url: '/asistencias/enviar',
                type: 'POST',
                data: dataString,
                dataType: 'JSON',
                beforeSend: function() {
                  $('#cargando').html("<img src='/images/loader.gif' />");
        },
                success: function (response) {
              Swal.fire({
              position: 'center',
              icon: 'success',
              title: response,
              showConfirmButton: false,
              timer: 1500
            });  
            $('#cargando').hide();
              }
              
             });
            

          */


</script>

@endsection