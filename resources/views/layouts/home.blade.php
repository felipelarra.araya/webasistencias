@extends('layouts.main')
@section('contenido')  

    <div class="container">
    <div class="alert alert-primary mt-2" role="alert">Cursos</div>      
    
<div class="row">



   @if(count($cursos)< 1  && (empty(Session::get('idPeriodo'))))
<div class="container">
<div class="jumbotron">
  <h1 class="display-5">Bienvenido {{ Auth::user()->name }}!</h1>
  <p class="lead">Comienza creando un periodo a trabajar, luego crea tus asignaturas para terminar creando cursos y alumnos.</p>
  <hr class="my-4">
  <p>Relaciona asignaturas a cursos, registra la asistencia de los estudiantes a clases y revisa su asistencia en determinadas fechas.</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="{{ url('/periodos')}}" role="button">Ir a Periodos</a>
  </p>
</div>

</div>
@endif



@if(Auth::user()->nivel == '1' && (Session::get('idPeriodo')))

<div class="col-md-12">
    <button type="button" data-toggle="modal" data-target="#modalCurso"  class="btn btn-outline-primary float-right"><i class='fa fa-plus'></i> Nuevo Curso</button>
                 </div>
                 @endif
                 </div>       
                        <div class="row mt-5">
                        @foreach( $cursos as $curso)
                        
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                @if(Auth::user()->nivel == '1')
                                <div class="col-md-12">
                                  <a href="" data-toggle="modal" data-target="#modalEliminar{{$curso->id}}"><div class="small text-white float-right"><i class="fas fa-trash"></i></div>
                                    </div>
                                    </a>
                                    @endif
                                    <div class="card-body">{{ $curso->nombre }}</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white"href="{{ url('alumnos/'.$curso->id)}}">Ver</a>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <!-- Modal Eliminar -->
<div class="modal fade" id="modalEliminar{{$curso->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Eliminar Curso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     

        <div class="modal-body text-center">
        <h5>¿Desea eliminar Curso?</h5>
        
        </div>
           
        <div class="modal-footer">
         
          <a type="button" class="btn btn-danger" href="{{ url('/asignaturasCurso/'.$curso->id.'/delete')}}">Si</a> 
          
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
 
    </div>
  </div>
</div>     
                            @endforeach
                           
                        </div>
                    
            <div class="row">
                <div class="col-12 d-flex justify-content-center pt-2">
                    {{$cursos->links()}}
                </div>
            </div>   
</div>


   


<div class="modal fade" id="modalCurso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center">Nuevo Curso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="form-group">
      <form method="post" name="form" onsubmit="validate(event, this)"; action="/asignaturasCurso" id="asignarForm">
    @csrf
    <div class="form-group">
    <label >Curso</label>
    <select name="nombreCurso" class="form-control" id="selectCurso" required>
    <option value="">Elige una opción</option>
      <option>1° Básico</option>
      <option>2° Básico</option>
      <option>3° Básico</option>
      <option>4° Básico</option>
      <option>5° Básico</option>
      <option>6° Básico</option>
      <option>7° Básico</option>
      <option>8° Básico</option>
      <option>1° Medio</option>
      <option>2° Medio</option>
      <option>3° Medio</option>
      <option>4° Medio</option>
    </select>
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">Nivel</label>
    <select name="nivel" class="form-control" id="exampleFormControlSelect1" required>
    <option value="">Elige una opción</option>
    <option>A</option>
    <option>B</option>
    </select>
  </div>
  
  
  
  </div>
  </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
