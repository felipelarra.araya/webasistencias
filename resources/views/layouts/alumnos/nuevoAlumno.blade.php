@extends('layouts.main')
@section('contenido')

<div class="col-md-12 mt-2">
<a href="{{ route('alumnos',['idCurso'=> $idCurso]) }}"><i class='fa fa-chevron-left '></i> Regresar</a>
</div>
<div class="container mt-2">
<div class="alert alert-primary mt-2" role="alert">Nuevo Alumno</div>

<form action="/alumnos" id="formNuevoAlumno" method="post">
@csrf

<div class="modal-body">
         
    <div class="form-group">
    <input type="text" name="nombre"  class="form-control nomAp" placeholder="Nombre del alumno" required maxlength="80">
  </div>
  <div class="form-group">
  <input type="text" name="apellidos" id="rut" class="form-control nomAp" placeholder="Apellidos del alumno" required maxlength="80">
  </div>
  <div class="form-group">
  <input type="text" name="rut" maxlength="12" onkeyup="checkRut(this)" class="form-control"  placeholder="Rut del alumno" required>
  </div>
  <div class="form-group px-1">
  <button class="btn btn-primary" type="submit">Guardar</button>
  </div>
<!--Guardarmos en id del curso en un hidden para mandarlo al controlador e insertarlo a la bd -->  
<input id="idCurso" name="idCurso" type="hidden" value="{{ $idCurso}}">
</form>
</div>
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h6 class="display-4 ">Cargar Excel</h6>
    <p class="lead">Sube un archivo excel para cargar alumnos rápidamente.</p>
    <p class="lead">Descarga el formato correcto que se encuentra en la parte inferior.</p>
  
    <form action ="{{route('subirExcel')}}" method="post" enctype="multipart/form-data">
@csrf

<div class="col-md-12 ">
<input type="file" name="excelAlumno" required>
</div>
<div class="col-md-8 mt-2">
<button class="btn btn-primary" type="submit">Subir Excel</button>
</div>
<input id="idCurso" name="idCurso" type="hidden" value="{{ $idCurso}}">
</form>
<div class="col-12-md">
<a href="{{route('descargarExcel')}}"><button class="btn btn-success float-right " type="button"><i class='fa fa-file-excel'></i> Descargar formato</button></a>
</div>
  </div>  

</div>


 <!-- Alumno Modal-->
 <div class="modal fade" id="alumnoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Alumno</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">¿Desea agregar otro alumno?</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Si</button>  
          <a type="button" class="btn btn-primary" href="{{ url('/alumnos/'.$idCurso)}}">No</a>
          
          
        </div>
      </div>
    </div>

</div>

@endsection

@section('scripts')

<script>



$(document).ready(function() {

  @if($message = Session::get('Listo'))
    $("#alumnoModal").modal("show");
    @endif

  @if(!empty($failures))
  @foreach ($failures as $failure)
  @foreach ($failure->errors() as $error)
  
  
  Swal.fire({
  position: 'center',
  icon: 'error',
  title: '{{ $error }}',
  showConfirmButton: true,
  
});      
@endforeach
@endforeach
  @endif
  
    $('.nomAp').bind('keypress', function(event) {
    var regex = new RegExp("^[a-zA-Z \u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
  if (!regex.test(key)) {
    event.preventDefault();
    return false;
  }
  });


   
});

function checkRut(rut)
{rut.value=rut.value.replace(/[.-]/g, '')
.replace( /^(\d{1,2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4')}
</script>

@endsection