@extends('layouts.main')
@section('contenido')


<div class="container">     
<div class="alert alert-primary mt-2" role="alert"> 
    @foreach($curso as $c)   
    <li class="breadcrumb-item active">{{ $c->nombre}}</li>
    @endforeach
</div>
<div class="col-md-12 ">
<a class="float-left" href="{{ route('home') }}"><i class='fa fa-chevron-left '></i> Regresar</a>
</div>

  <div class="container mt-5 col-md-8 col-sm-12">    
    <a href="{{ url('/alumnos/add/'.$c->id)}}"><button type="button" class="btn btn-outline-primary btn-block-xs-only"><i class='fa fa-plus'></i> Agregar Alumno</button></a>
    <button type="button" data-toggle="modal" data-target="#modalAsignaturas" class="btn btn-outline-primary btn-block-xs-only"><i class='fa fa-book'></i> Asignaturas</button>
    @if(count($asignaturas_cursos)>0)
    <a href="{{ url('/asistencias/'.$c->id)}}"><button type="button" class="btn btn-outline-primary btn-block-xs-only"><i class='fa fa-check'></i> Asistencia</button></a>
    <a href="{{ url('/reporte/'.$c->id)}}"><button  class="btn btn-outline-primary btn-block-xs-only"><i class='fa fa-table'></i> Reporte Asistencia</button></a>
    @endif
    </div>
    
    <table class="table col-12 table-bordered table-hover mt-4">
      <thead style=" background: #00bcd4;color: #fff;">
      
      @if(count($alumnos)>0)
        <tr>
          
          <th class="text-center" scope="col">Nombre</th>
          <th class="text-center" scope="col">Apellidos</th>
          <th class="text-center" scope="col">Rut</th>
          <th class="text-center" scope="col">Acciones</th>
          
        </tr>
      </thead>
      <tbody>
      @foreach($alumnos as $alumno)
        <tr>
          <td>{{ $alumno->nombre }}</td>
          <td>{{ $alumno->apellidos}}</td>
          <td>{{ $alumno->rut}}</td>
        </a>
          <td>
          <button type="button"class="btn btn-round btnEditar"data-toggle="modal" data-target="#modalEditar{{$alumno->id}}"><i class="fa fa-edit"></i>
          </button>
          <button class="btn btn-round btnEliminar" data-toggle="modal" data-target="#modalEliminar{{$alumno->id}}"><i class="fa fa-trash"></i> 
          </button>
          </td>

  <!-- Modal Eliminar -->
<div class="modal fade" id="modalEliminar{{$alumno->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Eliminar Alumno</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     

        <div class="modal-body">
        <h5 class="col-11 text-center">¿Desea eliminar el alumno?</h5>
        
        </div>
           
        <div class="modal-footer">
          <a type="button" class="btn btn-danger tamañoBoton"  href="{{ url('/alumnos/'.$alumno->id.'/delete')}}">Si</a> 
          <button type="button" class="btn btn-secondary tamañoBoton"   data-dismiss="modal">No</button>
        </div>
 
    </div>
  </div>
</div>         
 
 <div class="modal fade" id="modalEditar{{$alumno->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Editar Alumno</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
        <div class="form-row">
    <div class="col-md-12 mb-3">
    <form action="/alumnos/edit" method="post">
      @csrf
    <label>Nombre</label>
      <input type="text" name="nombre" class="form-control" placeholder="Nombre del alumno" value="{{$alumno->nombre}}" required>
      
    </div>
    <div class="col-md-12 mb-3">
      <label>Apellidos</label>
      <input type="text" name="apellidos" id="rut" class="form-control" placeholder="Apellidos del alumno" value="{{$alumno->apellidos}}" required>
    </div>
    <div class="col-md-12 mb-3">
      <label>Rut</label>
      <div class="input-group">
        <input type="text" name="rut" maxlength="12" onkeyup="checkRut(this)" class="form-control"  placeholder="Rut del alumno" value="{{$alumno->rut}}" required>
      </div>
    </div>
  </div>
        <div class="modal-footer">
        <button class="btn btn-primary tamañoBoton"  type="submit">Guardar</button>
        <button type="button"  class="btn btn-secondary tamañoBoton" data-dismiss="modal">Cerrar</button> 
        <input type="hidden" name="id" id="idEdit" value="{{$alumno->id}}">
        <input id="idCurso" name="idCurso" type="hidden" value="{{ $alumno->id_curso}}">
        
        </div>
        </form>
    </div>
  </div>
</div>
</div>
        </tr>
        
@endforeach
</tbody>
    </table>
    
    <div class="row">
    <div class="col-12 d-flex justify-content-center pt-2">
    {{$alumnos->links()}}
    </div>
    </div>
@else
<div class="mt-2"> 
<h4>{{"No existen Alumnos"}}</h4>
</div>
@endif


 
<!-- Modal asignaturas -->
<div class="modal fade" id="modalAsignaturas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Asignar Asignaturas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
        <form action="/asignaturasCurso/modificarAsignaturasCurso" method="post">
      @csrf
        
        
        @foreach ($asignaturas_cursos as $nombre =>$id)
        <div class="form-check  form-check-inline">   
        <input class="form-check-input" name="asignaturasCurso[]" type="checkbox" checked value="{{$id}}"/> 
        <label class="form-check-label" for="defaultCheck1">
  {{$nombre}}
  </label>
  </div>
@endforeach

@foreach ($asignaturas as $noSeleccionados)
<div class="form-check  form-check-inline">  
           <input  class="form-check-input" type="checkbox" name="asignaturasCurso[]" value="{{ $noSeleccionados->id }}"/> 

           <label class="form-check-label" for="defaultCheck1">
           {{ $noSeleccionados->nombre }}
  </label>
</div>
           @endforeach

        
    
</div>
           
        <div class="modal-footer">
        
        @foreach($curso as $curso2)   
          <button type="submit" class="btn btn-primary tamañoBoton">Guardar</a> 
          <input type="hidden" value="{{$curso2->id}}" name="idCursoActual">
          @endforeach
          <button type="button" class="btn btn-secondary tamañoBoton" data-dismiss="modal">Cancelar</button>
        </div>
 </form>
    </div>
  </div>
</div>        
</div>                    









@endsection

@section('scripts')

<script>
  

  function checkRut(rut)
{
  rut.value=rut.value.replace(/[.-]/g, '').replace( /^(\d{1,2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4')
}


</script>

@endsection