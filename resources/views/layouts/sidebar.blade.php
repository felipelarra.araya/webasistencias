<div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Home</div>
                            <a class="nav-link" href="/home">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Inicio
                            </a>                    
                        @if(Auth::user()->nivel == '1')
                        
                        
                            <div class="sb-sidenav-menu-heading">Administración</div>
                            <a class="nav-link" href="/asignaturas">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Asignaturas
                            </a>
                            <a class="nav-link" href="/periodos">
                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Periodos
                            </a>
                           @endif
                        </div>
                    </div>
                  
                </nav>
            </div>