@extends('layouts.main')
@section('contenido')

    <div class="container mt-4">
 
    
<div class="alert alert-primary" role="alert">
  Cambiar Contraseña
</div>

<form method="post" action="/cambiarContraseña/edit">
@csrf
<div class="form-group">
    
    <input type="password" name="passwordActual" class="form-control"  placeholder="Contraseña actual">
  </div>
  <div class="form-group">
    
    <input type="password" name="password" class="form-control"  placeholder="Contraseña nueva">
  </div>

  <div class="form-group">
    
    <input type="password" name="password_confirmation" class="form-control"  placeholder="Confirmar contraseña">
  </div>

  <button type="submit" class="btn btn-primary">Cambiar Password</button>


</form>
    
</div>

@endsection

@section('scripts')
<script>
$(document).ready(function() {
    
   
    setTimeout(function() {
        $("#mensajeExito").fadeOut(1500);
    },3000);

   
});
</script>

@endsection