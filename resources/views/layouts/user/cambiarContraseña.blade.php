@extends('layouts.main')
@section('contenido')

    <div class="container mt-4">
 
    <div class="row">
        @if($message = Session::get('Listo'))
          <div class="col-12 alert alert-success alert-dismissable fade show" id="mensajeExito" role="alert"> 
        <span>{{ $message }}</span>  
        </div>
        @endif

        @if($message = Session::get('Error'))

            <div class="col-12 alert alert-danger alert-dismissable fade show" role="alert">
              <h5>Errores:</h5>
            <ul>
              @foreach($errors->all() as $error)
            <li>{{ $error }}</li>

            @endforeach
            </ul>  
            </div>
            @endif

            @if($message = Session::get('ErrorContraseña'))

            <div class="col-12 alert alert-danger alert-dismissable fade show" role="alert">
              <h5>Contraseña actual incorrecta:</h5>
            <ul>
              @foreach($errors->all() as $error)
            <li>{{ $error }}</li>

            @endforeach
            </ul>  
            </div>
            @endif
</div>
<div class="alert alert-primary" role="alert">
  Cambiar Contraseña
</div>

<form method="post" action="/cambiarContraseña/edit">
@csrf
<div class="form-group">
    
    <input type="password" name="passwordActual" class="form-control"  placeholder="Contraseña actual">
  </div>
  <div class="form-group">
    
    <input type="password" name="password" class="form-control"  placeholder="Contraseña nueva">
  </div>

  <div class="form-group">
    
    <input type="password" name="password_confirmation" class="form-control"  placeholder="Confirmar contraseña">
  </div>

  <button type="submit" class="btn btn-primary">Cambiar Password</button>


</form>
    
</div>

@endsection

@section('scripts')
<script>
$(document).ready(function() {
    
   
    setTimeout(function() {
        $("#mensajeExito").fadeOut(1500);
    },3000);

   
});
</script>

@endsection