@extends('layouts.main')
@section('contenido')

    <div class="container mt-4">
 
  
<div class="alert alert-primary" role="alert">
  Asignaturas
</div>

@if(Session::get('idPeriodo'))
    <button type="button"  data-toggle="modal" data-target="#modalAsignatura" class="btn btn-outline-primary"><i class='fa fa-plus'></i> Nueva Asignatura</button>      
    @endif
    <table class="table col-12 table-bordered table-hover mt-2">
      <thead>
      
      @if(count($asignaturas)>0)
        <tr>
          
          <th class="table-primary" scope="col">Nombre</th>
          <th class="table-primary" scope="col">Acciones</th>
          
        </tr>
      </thead>
      <tbody>
      @foreach($asignaturas as $asignatura)
        <tr>
          <td >{{ $asignatura->nombre }}</td>
        </a>
          <td>
          <button type="button"class="btn btn-round btnEditar"data-toggle="modal" data-target="#modalEditar{{$asignatura->id}}"><i class="fa fa-edit"></i>
          </button>
          <button class="btn btn-round btnEliminar" data-toggle="modal" data-target="#modalEliminar{{$asignatura->id}}"><i class="fa fa-trash"></i> 
          </button>
          </td>

          
  <!-- Modal Eliminar -->
<div class="modal fade" id="modalEliminar{{$asignatura->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Eliminar Asignatura</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     

        <div class="modal-body text-center">
        <h5>¿Desea eliminar la asignatura?</h5>
        
        </div>
           
        <div class="modal-footer">
          <a type="button" class="btn btn-danger tamañoBoton"  href="{{ url('/asignaturas/'.$asignatura->id.'/delete')}}">Si</a> 
          <button type="button" class="btn btn-secondary tamañoBoton"  data-dismiss="modal">No</button>
        </div>
 
    </div>
  </div>
</div>
<!-- Modal editar-->
<div class="modal fade" id="modalEditar{{$asignatura->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Modificar Asignatura</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
    <div class="form-row">
    <div class="col-md-12 mb-3">
    <form action="/asignaturas/edit" method="post">
      @csrf
    <label>Nombre</label>
      <input type="text" name="nombre" class="form-control" placeholder="Nombre Asignatura" value="{{ $asignatura->nombre }}"  required>
    </div>
</div>
</div>
        <div class="modal-footer">
         <!-- Mandamos el id de la asignatura a editar -->   
        <input type="hidden" name="id" id="idEdit" value="{{$asignatura->id}}">
        <button class="btn btn-primary tamañoBoton"  type="submit">Guardar</button>   
        <button type="button" class="btn btn-secondary tamañoBoton"  data-dismiss="modal">Cerrar</button> 
        
        </div>
        </form>
    </div>
  </div>
</div>
          </tr>
        
        @endforeach
        </tbody>
            </table>
            {{$asignaturas->links()}}
@else
<div class="mt-1 col-md-4  text-center"> 
<h4>{{"No existen Registros"}}</h4>
</div>

@endif

</div>



<div class="modal fade" id="modalAsignatura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title col-11 text-center" id="exampleModalLabel">Agregar Asignatura</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
    <div class="form-row">
    <div class="col-md-12 mb-3">
    <form action="/asignaturas" method="post">
      @csrf
    <label>Nombre</label>
      <input type="text" name="nombre" class="form-control" placeholder="Nombre Asignatura"  required>
    </div>
</div>
</div>
        <div class="modal-footer">
        <button class="btn btn-primary tamañoBoton" type="submit">Guardar</button>   
        <button type="button" class="btn btn-secondary tamañoBoton" data-dismiss="modal">Cerrar</button> 
        
        </div>
        </form>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
$(document).ready(function() {
    
   
    setTimeout(function() {
        $("#mensajeExito").fadeOut(1500);
    },3000);

   
});
</script>

@endsection