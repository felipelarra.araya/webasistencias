@extends('layouts.main')
@section('contenido')  

                <div class="container mt-4">
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Asignar asignaturas al Curso</li>
                        </ol>
                        <form method="post" action="/asignaturasCurso" id="asignarForm">
                        @csrf
                        <div class="row">
                        @foreach( $asignaturas as $asignatura)
                        
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                
                                    <div class="card-body">{{ $asignatura->nombre }}</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                    <div class="form-check">
                                      
                                    <input type="checkbox" class="form-check-input" id="asignacionCheck" name="asignacion[]" value="{{ $asignatura->id}}">
                                    <label class="form-check-label" for="exampleCheck1">Asignar</label>
                                   
                                </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        
                        <input id="idCurso" name="idCurso" type="hidden" value="{{ $idCursoRecibido}}">
                      
                        <button type="submit" class="btn btn-outline-primary"><i class='fa fa-check-circle'></i> Enviar</button>
                        </form>
</div>

@endsection